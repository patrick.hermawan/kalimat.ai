from rest_framework import serializers
from .models import Reviews
from rest_framework.pagination import PageNumberPagination

class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reviews
        fields = '__all__'

class ReviewListPagination(PageNumberPagination):
    page_size = 10,
    page_size_query_param = 'limit'