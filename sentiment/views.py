from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView

from .models import Reviews
from .serializer import ReviewSerializer
from .serializer import ReviewListPagination

from .ai import train
from .ai import predict

from django.http import JsonResponse
# Create your views here.




class AllReviews(ListAPIView):
    queryset = Reviews.objects.all()
    serializer_class = ReviewSerializer
    pagination_class = ReviewListPagination

    def post(self, request, format=None):
        serializer = ReviewSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReviewView(APIView):
    def get_object(self, pk):
        try:
            return Reviews.objects.get(pk=pk)
        except Reviews.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        review = self.get_object(pk)
        serializer = ReviewSerializer(review)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        review = self.get_object(pk)
        serializer = ReviewSerializer(review, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        review = self.get_object(pk)
        review.delete()
        return Response(status=status.HTTP_200_OK)

class Train(APIView):
    def get(self, request, format=None):
        queryset = Reviews.objects.all()
        train(queryset)
        return Response(status=status.HTTP_200_OK)

class Predict(APIView):
    def get(self, request, format=None):
        if 'sentence' in request.GET:
            sentence = request.GET['sentence']
            sentiment = predict(sentence)
            if sentiment == "pos":
                return JsonResponse({'sentiment':'positive'})
            if sentiment == "neg":
                return JsonResponse({'sentiment':'negative'})
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)