import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords

import random
import pickle

from rest_framework.response import Response
from rest_framework import status


def prepare_nltk():
    nltk.download('stopwords')
    nltk.download('punkt')

def save_classifier(classifier):
   f = open('classifier.pickle', 'wb')
   pickle.dump(classifier, f, -1)
   f.close()

def load_classifier():
   f = open('classifier.pickle', 'rb')
   classifier = pickle.load(f)
   f.close()
   return classifier

def word_feats(words,all_words = set()):  # assigning whether each word from the dictionary exists or not in each particular review
    tmp = dict()
    for word in all_words:
        tmp[word] = False
    for word in words:
        tmp[word] = True
    return tmp

def train(listofreviews):
    prepare_nltk() # download the necessary database (stop words)
    new_listofreviews = [] # this would be the one to feed to NLTK trainer

    stop_words = set(stopwords.words('english'))
    ps = PorterStemmer() # stemmer, to take into account the suffixes/prefixes
    all_words = set() # our dictionary
    for review in listofreviews:
        tokens = word_tokenize(review.sentence) #tokenize the review, basically splitting each word into separate strings

        filtered = []
        for token in tokens:
            if token not in stop_words: # stop words are skipped
                all_words.add(token) # adding the word to the dictionary (since dictionary type is SET, we don't need to worry about multiple entries)
                filtered.append(ps.stem(token)) # the words are stemmed, to take into account the suffixes/prefixes
        new_listofreviews.append((filtered,review.label)) # each item in new_listofreviews is a pair of the tokenized words, and the sentiment (pos or neg)
    print("dictionary created successfully")

    new_listofreviews = [(word_feats(new_review[0],all_words), new_review[1]) for new_review in new_listofreviews]

    val = []
    train = []
    for new_review in new_listofreviews:
        if random.uniform(1, 10) > 8.5:  # approximately 15% of the data is used for validation
            val.append(new_review)
        else:
            train.append(new_review)

    print('train on %d instances, test on %d instances' % (len(train), len(val)))

    classifier = NaiveBayesClassifier.train(train)
    print('accuracy:', nltk.classify.util.accuracy(classifier, val))
    classifier.show_most_informative_features() # show the most informative/decisive words

    save_classifier(classifier) # saving the model for future prediction


def predict(sentence):
    prepare_nltk() # download the necessary database (stop words)
    try:
        classifier = load_classifier()
    except:
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR) # if the classifier file cannot be loaded, then send an error response
    stop_words = set(stopwords.words('english'))
    ps = PorterStemmer()

    tokens = word_tokenize(sentence)

    filtered = []
    for token in tokens:
        if token not in stop_words:
            filtered.append(ps.stem(token)) # similar to training, the words are tokenized, filtered from stop words, and stemmed

    filtered = (word_feats(filtered))

    return classifier.classify(filtered)
