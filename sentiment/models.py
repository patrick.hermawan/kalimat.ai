from django.db import models

# Create your models here.
class Reviews(models.Model):
    def __getitem__(self, index):
        return self.id

    SENTIMENT = (
        ('neg', 'negative'),
        ('pos', 'positive'),
    )
    label = models.CharField(max_length=1, choices=SENTIMENT)
    sentence = models.CharField(max_length=20000)