# kalimat.ai

"Task for ML Software Engineer in Test (1).pdf"

## Description

### CRUD FUNCTIONALITY
By using REST API, user should be able to list/insert/update/delete training data.
1. User should be able to see the existing training data by sending GET /data?limit=\<number>&page=\<number> request.
    * The training data can be stored in any format, either using RDBMS/No-SQL/Csv-File/JSON-File, etc.
    * The response should be in JSON format (e.g: [{“id”: 1, “sentence”: “I am happy”, “label”: 1}, {“id”:2, “sentence”: “I am sad”, “label”: 0}]).
2.	User should be able to insert new training data by sending POST /data request with JSON payload (e.g: 
{“sentence”: “I love Friday”, “label”: 1}).
3.	User should be able to update the existing training data by sending PUT /data/<id> request with JSON payload 
(e.g: {“sentence”: “I don't really love Friday :(“, “label”: 0}).
4.	User should be able to delete the existing training data by sending DELETE /data/<id> request.
5.	For initial training-set, you can export movie review data-set (see resources) into your preferable format (refer to point 1.a)

Note: in the example, we assume label: 0 to be negative and label:1 to be positive.

**In this project, I used label:neg to be negative and label:pos to be positive.**

### TRAINING  & SENTIMENT ANALYSIS FUNCTIONALITY
Kalimat.ai's research team has already build simple program to classify sentence based on Naive-Bayes Classifier (see Resources). However, we have some problems related to the program:
* It depends on movie_reviews data-set (provided by NLTK). However, we want to use our own training set instead of NLTK's corpus.
* The program only show the collective testing accuracy instead of real classification result.
* The training and testing/classification process is inseparable.

Since training process takes a lot of time/CPU/memory, we don't want to do it every time. Instead, we want to generate and reuse a pre-train model. Thus, we define the specification as follow:
1.	Given GET /train request, the program will generate a classification model
2.	Given GET /sentiment?sentence=\<a sentence>, the program should give classification response in JSON format (i.e: {sentiment: 1}).

**Unlike the note above, the JSON response follows the convention sentiment:negative for negative and sentiment:positive for positive.** (see "Quick Start" section below)

## Prerequisites

* [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html)
* (recommended) [Git](https://git-scm.com/downloads)
* (recommended) [Postman](https://www.getpostman.com/downloads/) for testing
* (recommended, only tested on) Latest version of [Windows 10](https://www.microsoft.com/en-us/software-download/windows10)

## Setting up

### Downloading Package

1. Download the kalimat.ai package
    * (recommended) Using Git:
        1. Install [Git from the official website](https://git-scm.com/downloads)
        2. Open Git Bash
        3. Decide which folder you want this **project's folder in**. I will use **D:\Git_Projects** in this example
        4. Type **cd D:/Git_Projects**
        5. Type **git clone https://gitlab.com/patrick.hermawan/kalimat.ai.git**
        6. The project will be placed in D:/Git_Projects/kalimat.ai
    * (not recommended) Without Git:
        *  [Click the cloud button](https://gitlab.com/patrick.hermawan/kalimat.ai), and download the project as ZIP or other format, extract it

### Preparing the environment

1. Download and install [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html) 
7. Open Anaconda Prompt
9. Move to the directory of the project by typing **cd /d D:/Git_Projects/kalimat.ai**
8. Install all the necessary components by typing **conda env create -f environment.yaml**
9. Conda will output the environment path. In this example it would be **C:\Users\\%userprofile%\\.conda\envs\kalimatai**
    * Save the environment path, this would be useful to set up an interpreter in your IDE (e.g VS Code, PyCharm, etc.)
10. Activate the environment by typing **conda activate kalimatai**
11. Update the migrations by typing **python manage.py makemigrations**
    * If the command outputs *No changes detected*, it is totally fine, you can carry on to the next step
12. Apply the migration files to the database by tpying **python manage.py migrate**

### Installing fixtures (optional)

*(if you still have the Anacconda window open from the previous step, skip step 1-3 below)*

1. Open Anaconda Prompt
10. Activate the environment by typing **conda activate kalimatai**
2. Move to the directory of the project by typing **cd /d D:/Git_Projects/kalimat.ai**
3. Launch the server by typing **python manage.py runserver**, keep it running
4. Open another Anaconda window 
10. Activate the environment by typing **conda activate kalimatai**
5. Move to the directory of the project by typing **cd /d D:/Git_Projects/kalimat.ai**
5. Apply the fixtures by typing **python install_fixtures.py**

## Quick Start

*(if you still have the server running from the previous step, skip step 1-4 below)*

1. Open Anaconda Prompt
10. Activate the environment by typing **conda activate kalimatai**
2. Move to the directory of the project by typing **cd /d D:/Git_Projects/kalimat.ai**
3. Launch the server by typing **python manage.py runserver**
4. Open **Postman** application, close the intro window, and set up a new GET request
5. Type in **http://127.0.0.1:8000/sentiment**
6. In the **Params** tab, type **sentence** in the KEY column, and the sentence that you wish to analyze in the VALUE column
    * if you need some examples, you could find examples in the **txt_sentoken** folder, separated by the postive and negative folder
7. Press the big blue SEND button
8. You shall see the response, either **{"sentiment": "negative"}** or **{"sentiment": "positive"}**
