import requests
import os

def post_me(sentence):
    with requests.Session() as s:
        p = s.post(
            "http://127.0.0.1:8000/data",
            data={
                "label": 'pos',
                "sentence": sentence
                },
            headers = {'User-Agent': 'Mozilla/5.0'}
            )
        print(p)

def positive():
    for filename in os.listdir(os.path.dirname(os.path.abspath(__file__))):
        if filename == '__pycache__': continue
        filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),filename)
        sentence = open(filename).read()
        sentence = sentence.replace('\n', ' ')
        post_me(sentence)